<?php

namespace Drupal\performance_budget\Plugin\CaptureUtility;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\performance_budget\Event\WebPageTestCaptureJobCompleteEvent;
use Drupal\performance_budget\Plugin\CaptureResponse\WebPageTestCaptureResponse;
use Drupal\web_page_archive\Plugin\ConfigurableCaptureUtilityBase;
use WidgetsBurritos\WebPageTest\WebPageTest;

/**
 * Skeleton capture utility, useful for creating new plugins.
 *
 * @CaptureUtility(
 *   id = "pb_wpt_capture",
 *   label = @Translation("Web page test capture utility", context = "Web Page Archive"),
 *   description = @Translation("Runs url through webpagetest.org.", context = "Web Page Archive")
 * )
 */
class WebPageTestCaptureUtility extends ConfigurableCaptureUtilityBase {

  /**
   * Most recent response.
   *
   * @var string|null
   */
  private $response = NULL;

  /**
   * {@inheritdoc}
   */
  public function capture(array $data = []) {
    // Configuration data is stored in $this->configuration. For example:
    $wpt_api = $this->configuration['wpt_api'];
    $wpt = new WebPageTest($wpt_api);
    $state_key = $this->getStateKey($data);
    $test_id = $this->state()->get($state_key);
    $response_content = '';
    if (!isset($test_id)) {
      if ($response = $wpt->runTest($data['url'])) {
        if ($response->statusCode == 200) {
          $this->state()->set($state_key, $response->data->testId);
        }
      }

      $this->response = NULL;
    }
    else {
      if ($response = $wpt->getTestStatus($test_id)) {
        if ($response->statusCode == 200) {
          // Test is complete.
          if ($response = $wpt->getTestResults($test_id)) {
            $scheme = \Drupal::config('system.file')->get('default_scheme');
            $file_path = \Drupal::service('file_system')->realpath("{$scheme}://");
            $save_dir = "{$file_path}/performance-budget/wpt/{$data['web_page_archive']->id()}/{$data['run_uuid']}";
            $file_name = preg_replace('/[^a-z0-9]+/', '-', strtolower($data['url'])) . '.json';
            $file_location = "{$save_dir}/{$file_name}";

            \Drupal::service('file_system')->prepareDirectory($save_dir, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);

            file_put_contents($file_location, json_encode($response->data));
            $this->response = new WebPageTestCaptureResponse($file_location, $data['url']);

            $replacements = $this->response->getReplacementVariables($data);
            $this->notify('capture_complete_single', $replacements);

            // Notify about any violations.
            $violations = $this->response->getThresholdViolations($data['run_entity']);
            if (!empty($violations)) {
              $violation_messages = [];
              foreach ($violations as $violation) {
                $violation_replacements = [
                  '@kpi' => $violation['kpi'],
                  '@threshold' => $violation['threshold'],
                  '@actual' => $violation['actual'],
                ];
                switch ($violation['type']) {
                  case 'minimum':
                    $violation_messages[] = $this->t('[@kpi] @actual < @threshold', $violation_replacements);
                    break;

                  case 'maximum':
                    $violation_messages[] = $this->t('[@kpi] @actual > @threshold', $violation_replacements);
                    break;

                  default:
                    $violation_messages[] = $this->t('[@kpi] Invalid violation type: @type', ['@type' => $violation['type']]);
                }
              }
              $replacements['@violations'] = implode(PHP_EOL, $violation_messages);
              $this->notify('pb_threshold_violation', $replacements);
            }

            $event = new WebPageTestCaptureJobCompleteEvent($data['run_entity']);
            $event_dispatcher = \Drupal::service('event_dispatcher');
            $event_dispatcher->dispatch($event::EVENT_NAME, $event);
          }
          else {
            throw new \Exception($this->t('WPT test @test_id failed - Could not retrieve test results', ['@test_id' => $test_id]));
          }
          // Cleanup old state key.
          $this->state()->delete($state_key);
        }
        elseif (in_array($response->statusCode, [100, 101, 102])) {
          // Test is still running.
          $this->response = NULL;
        }
        else {
          // Test failed.
          $strings = [
            '@test_id' => $test_id,
            '@http_code' => $response->statusCode,
          ];
          throw new \Exception($this->t('WPT test @test_id failed - HTTP status code: @http_code', $strings));
        }
      }
    }

    return $this;
  }

  /**
   * Retrieves unique state key for data array.
   */
  private function getStateKey(array $data = []) {
    return "pb_wpt_capture:{$data['run_uuid']}:{$data['url']}";
  }

  /**
   * Retrieves unique state key for data array.
   */
  private function state() {
    return \Drupal::state();
  }

  /**
   * Retrieves config factory service.
   */
  private function configFactory() {
    return \Drupal::configFactory();
  }

  /**
   * Retrieves the storage for wpt_kpi entities.
   */
  private function getKpiStorage() {
    return \Drupal::entityTypeManager()->getStorage('wpt_kpi');
  }

  /**
   * Retrieves the kpi helper service.
   */
  private function getKpiHelper() {
    return \Drupal::service('performance_budget.helper.kpi');
  }

  /**
   * {@inheritdoc}
   */
  public function getResponse() {
    return $this->response;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = $this->configFactory()->get("web_page_archive.{$this->getPluginId()}.settings");
    $ret = [
      'wpt_api' => $config->get('defaults.wpt_api'),
      'autogen' => $config->get('defaults.autogen') ?: [
        'enabled' => FALSE,
        'date_range' => '',
        'date_range_start' => '',
        'date_range_end' => '',
      ],
      'kpi_groups' => $config->get('defaults.kpi_groups'),
      'chartjs_option' => $config->get('defaults.chartjs_option') ?: $this->getExampleChartJsOptions(),
    ];
    $this->injectNotificationDefaultValues($ret, $config->get('defaults') ?: []);
    return $ret;
  }

  /**
   * Retrieves example chart.js options. Also default value for new jobs.
   *
   * @return string
   *   String containing example javascript object.
   */
  private function getExampleChartJsOptions() {
    return "{
      title: {
        display: true,
        text: Drupal.t('{@group}: @url', {'@group': group, '@url': url}),
      },
      scales: {
        xAxes: [{
          type: 'time',
          distribution: 'linear',
          ticks: {
            source: 'data',
            autoSkip: true,
          },
          scaleLabel: {
            display: true,
            labelString: 'Date/Time'
          },
          time: {
            parser: 'MM/DD/YYYY HH:mm',
            tooltipFormat: 'll - h:mma'
          },
        }],
        yAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'Time (seconds)'
          },
        }],
      },
      plugins: {
        zoom: {
          zoom: {
            enabled: true,
            drag: true,
            mode: 'x',
            speed: 0.05,
          },
        },
      },
    }";
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['wpt_api'] = [
      '#type' => 'textfield',
      '#title' => $this->t('webpagetest.org API Key'),
      '#description' => $this->t('Enter your webpagetest.org API Key. http://www.webpagetest.org/getkey.php'),
      '#default_value' => isset($this->configuration['wpt_api']) ? $this->configuration['wpt_api'] : $this->defaultConfiguration()['wpt_api'],
      '#required' => TRUE,
    ];

    $form['autogen'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Autogenerate historical report settings'),
      '#tree' => TRUE,
      'enabled' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Enabled?'),
        '#description' => $this->t('Use this checkbox to force the regeneration of the historical report (i.e.  the visual trend graph) upon every capture.'),
        '#default_value' => isset($this->configuration['autogen']['enabled']) ? $this->configuration['autogen']['enabled'] : $this->defaultConfiguration()['autogen']['enabled'],
      ],
      'date_range' => [
        '#type' => 'select',
        '#title' => $this->t('Autogeneration date range'),
        '#description' => $this->t('Use this field to determine the time frame you wish to generate a chart for on the historical report.'),
        '#options' => [
          'all' => $this->t('All time'),
          'week' => $this->t('Past week'),
          'month' => $this->t('Past month'),
          '3month' => $this->t('Past 3 months'),
          '6month' => $this->t('Past 6 months'),
          'year' => $this->t('Past year'),
          '2year' => $this->t('Past 2 years'),
          'custom' => $this->t('Custom date range'),
        ],
        '#default_value' => isset($this->configuration['autogen']['date_range']) ? $this->configuration['autogen']['date_range'] : $this->defaultConfiguration()['autogen']['date_range'],
        '#states' => [
          'visible' => [
            ':input[name="data[autogen][enabled]"]' => ['checked' => TRUE],
          ],
          'required' => [
            ':input[name="data[autogen][enabled]"]' => ['checked' => TRUE],
          ],
        ],
      ],
      'date_range_start' => [
        '#type' => 'date',
        '#title' => $this->t('From:'),
        '#default_value' => isset($this->configuration['autogen']['date_range_start']) ? $this->configuration['autogen']['date_range_start'] : '',
        '#states' => [
          'visible' => [
            ['select[name="data[autogen][date_range]"]' => ['value' => 'custom']],
          ],
          'required' => [
            ['select[name="data[autogen][date_range]"]' => ['value' => 'custom']],
          ],
        ],
      ],
      'date_range_end' => [
        '#type' => 'date',
        '#title' => $this->t('To:'),
        '#default_value' => isset($this->configuration['autogen']['date_range_end']) ? $this->configuration['autogen']['date_range_end'] : '',
        '#states' => [
          'visible' => [
            ['select[name="data[autogen][date_range]"]' => ['value' => 'custom']],
          ],
          'required' => [
            ['select[name="data[autogen][date_range]"]' => ['value' => 'custom']],
          ],
        ],
      ],
    ];

    $kpi_add_link = Link::fromTextAndUrl($this->t('Create a new KPI group.'), Url::fromRoute('entity.wpt_kpi.add_form'));

    // Flatten group ids.
    $kpi_group_ids = isset($this->configuration['kpi_groups']) ? array_map(function ($value) {
      return $value['target_id'];
    }, $this->configuration['kpi_groups']) : [];
    $kpi_groups = !empty($kpi_group_ids) ? $this->getKpiStorage()->loadMultiple($kpi_group_ids) : NULL;

    $form['kpi_groups'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'wpt_kpi',
      '#title' => $this->t('KPI Groups'),
      '#description' => $this->t('Select which KPI groups to apply to this job. @link', ['@link' => $kpi_add_link->toString()]),
      '#tags' => TRUE,
      '#default_value' => $kpi_groups,
      '#maxlength' => NULL,
      '#required' => TRUE,
    ];
    $chartjs_url = 'https://www.chartjs.org/docs/latest/';
    $form['chartjs_option'] = [
      '#type' => 'textarea',
      '#description' => $this->t('Use this field to define the chart options as a javascript object. You are allowed to use existing javascript functions, such as <code>Drupal.t()</code>, the <code>url</code> variables is available to represent the captured URL, and the <em>group</em> variable is available to represent the KPI group. See <a href="@url" target="_blank" rel="noopener noreferrer">Chart.js documentation</a> for more information about available options.', ['@url' => $chartjs_url]),
      '#default_value' => isset($this->configuration['chartjs_option']) ? $this->configuration['chartjs_option'] : $this->defaultConfiguration()['chartjs_option'],
      '#required' => TRUE,
      '#attached' => [
        'library' => [
          'performance_budget/chartjs-validate',
        ],
      ],
    ];
    $form['chartjs_example'] = [
      '#type' => 'html_tag',
      '#tag' => 'pre',
      '#prefix' => $this->t('For example:'),
      '#value' => $this->getExampleChartJsOptions(),
    ];
    $form['chartjs_display'] = [
      '#type' => 'html_tag',
      '#tag' => 'canvas',
      '#prefix' => $this->t('Sample chart using above settings:'),
      '#attributes' => [
        'id' => 'wpt_kpi_chart_preview',
        'style' => [
          'border: 1px dotted #c8c8c8;',
          'display: block;',
        ],
      ],
      '#value' => '',
    ];

    $this->injectNotificationFields($form, $this->configuration);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $keys_to_save = [
      'wpt_api' => TRUE,
      'chartjs_option' => TRUE,
      'kpi_groups' => FALSE,
    ];
    foreach ($keys_to_save as $key => $trim) {
      $this->configuration[$key] = $trim ? trim($form_state->getValue($key)) : $form_state->getValue($key);
    }
    $this->configuration['autogen'] = $form_state->getValue('autogen');

    $this->injectNotificationConfig($this->configuration, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getNotificationContexts() {
    $contexts = parent::getNotificationContexts();
    unset($contexts['capture_complete_all']);
    $contexts['pb_threshold_violation'] = [
      'label' => $this->t('Performance Budget Threshold Violation'),
      'description' => $this->t('This context occurs when a threshold violation is detected during a capture.'),
    ];
    return $contexts;
  }

  /**
   * {@inheritdoc}
   */
  public function getReplacementListByContext($context) {
    $ret = [
      '@wpa_id' => $this->t('Web page archive configuration entity ID'),
      '@wpa_label' => $this->t('Web page archive configuration entity label'),
      '@wpa_run_id' => $this->t('Web page archive run entity ID'),
      '@wpa_run_label' => $this->t('Web page archive run entity label'),
      '@wpa_run_url' => $this->t('URL to this individual run'),
    ];

    switch ($context) {
      case 'capture_complete_single':
      case 'pb_threshold_violation':
        $ret['@url'] = $this->t('URL of the current run');
        if ($context == 'pb_threshold_violation') {
          $ret['@violations'] = $this->t('List of violations');
        }
        $kpi_helper = $this->getKpiHelper();
        $kpi_groups = $this->getKpiStorage()->loadMultiple();
        foreach ($kpi_groups as $kpi_group_id => $kpi_group) {
          foreach ($kpi_group->getKpis() as $average => $average_details) {
            foreach ($average_details as $view => $view_details) {
              foreach ($view_details as $metric => $active) {
                // Skip threshold metrics.
                if (strpos($metric, '_threshold') !== FALSE) {
                  continue;
                }
                if ($active) {
                  $tokens = [
                    'pb_wpt',
                    $kpi_group->label(),
                    $kpi_helper->getFormattedAverageValue($average),
                    $kpi_helper->getFormattedViewValue($view),
                    $kpi_helper->getKpiMap()[$metric]['title'],
                  ];
                  $key = $kpi_helper->getReplacementKey($tokens);
                  $replacements = [
                    '@group' => $tokens[1],
                    '@average' => $tokens[2],
                    '@view' => $tokens[3],
                    '@metric' => $tokens[4],
                  ];
                  if ($key) {
                    $ret[$key] = $this->t('KPI Group Results [@group : @average : @view : @metric]', $replacements);
                  }
                }
              }
            }
          }
        }
        break;
    }
    return $ret;
  }

}
